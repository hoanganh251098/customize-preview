$(document).on('click', '.go-btn, .download-btn', function() {
    const btnTypeGo = $(this).hasClass('go-btn');
    console.log(this);
    const productName = $('.product-name-input').val();
    const dataComponentNames = [
        'data.json',
        'bgColor.json',
        'bgImage.json',
        'map.json',
        'mapMask.json',
        'mask.json',
        'song.json',
        'text.json'
    ];
    function downloadURI(uri, name) {
        var link = document.createElement("a");
        link.download = name;
        link.href = uri;
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
        delete link;
    }

    $.ajax({
        url: '/set-product-location',
        type: 'post',
        contentType: 'application/json',
        data: JSON.stringify({
            location: productName,
        }),
        success(res) {
            $.ajax({
                url: '/product/data.json',
                type: 'get',
                dataType: 'json',
                success(res) {
                    if(btnTypeGo) {
                        window.location = '/product/index';
                    }
                    else {
                        downloadURI('/product/data.json', 'data.json');
                    }
                    console.log(res);
                },
                error() {
                    let loaded = 0;
                    const checkFullyLoaded = () => {
                        loaded += 1;
                        if(loaded == dataComponentNames.length) {
                            $(document).trigger('AllComponentsLoaded');
                        }
                    };
                    const components = [];
                    $(document).one('AllComponentsLoaded', () => {
                        const dataJSON = components[0];
                        for(const component of components.slice(1)) {
                            if(component != null) {
                                for(const subComponent of component) {
                                    dataJSON.children.push(subComponent);
                                }
                            }
                        }
                        $.ajax({
                            url: '/set-data-json',
                            type: 'post',
                            contentType: 'application/json',
                            data: JSON.stringify(dataJSON),
                            success(res) {
                                if(btnTypeGo) {
                                    window.location = '/product/index';
                                }
                                else {
                                    downloadURI('/product/data.json', 'data.json');
                                }
                            },
                            error(err) {
                                console.log(err);
                            }
                        });
                    });
                    for(const [componentId, componentName] of dataComponentNames.entries()) {
                        components.push(null);
                        $.ajax({
                            url: '/product/processing/' + componentName,
                            type: 'get',
                            dataType: 'text',
                            success(res) {
                                try {
                                    if(componentId > 0) {
                                        components[componentId] = JSON.parse('[' + res.slice(0, res.lastIndexOf(','))+ ']');
                                    }
                                    else {
                                        components[componentId] = JSON.parse(res);
                                    }
                                }
                                catch(e) {
                                    alert(`Error at component '${componentName}':\n${e.message}`);
                                }
                                checkFullyLoaded();
                            },
                            error() {
                                checkFullyLoaded();
                            }
                        });
                    }
                }
            });
        }
    });
});