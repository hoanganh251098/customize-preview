const pug       = require('pug');
const express   = require('express');
const busboy    = require("connect-busboy");
const fs        = require('fs');
const uuid      = require("uuid");
const cors      = require('cors');
const path      = require('path');
const https     = require('https');
const http      = require('http');
const open      = require('open');

const router = express();
router.use(express.json());
router.use(busboy());
router.use(cors());
router.use('/assets', express.static('public'));

function isFileExists(path) {
    try {
        if (fs.existsSync(path)) {
            return true;
        }
        return false;
    } catch(err) {
        return false;
    }
}

router.get('(/:page)?', function(req, res) {
    const page = req.params.page || 'index';
    const filePath = `./templates/${page}.pug`;

    const pugPath = isFileExists(`./templates/${page}.pug`) && `./templates/${page}.pug`;
    const htmlPath = isFileExists(`./templates/${page}.html`) && `./templates/${page}.html`;
    if(pugPath) {
        res.send(
            pug.compileFile(filePath)({
                time: (new Date).getTime()
            })
        );
    }
    else if(htmlPath) {
        res.sendFile(htmlPath, { root: __dirname });
    }
    else {
        return res.sendStatus(404);
    }
});

function proxy(req, res, path) {
    if(!path.includes('http')) {
        const slashedPath = path[0] == '/' ? path : '/' + path;
        https.get('https://dev.goodsmize.com'+slashedPath, function(response) {
            const headers = response.headers;
            res.set('content-type', headers['content-type']);
            res.status(response.statusCode);
            response.on('data', data => {
                res.write(data);
            });
            response.on('end', () => {
                res.end();
            });
        });
    }
    else {
        https.get(path, function(response) {
            const headers = response.headers;
            res.set('content-type', headers['content-type']);
            res.status(response.statusCode);
            response.on('data', data => {
                res.write(data);
            });
            response.on('end', () => {
                res.end();
            });
        });
    }
}

router.get('/product-root/:path([a-zA-Z0-9-_\/\\\.\%\: ]+)', function(req, res) {
    if(isFileExists(req.params.path)) {
        res.sendFile(decodeURI(req.params.path));
    }
    else {
        proxy(req, res, 'https://dev.goodsmize.com/design-upload-v2/products/' + req.params.path);
    }
});

router.get(':path([a-zA-Z0-9-_\/\\\.\%\: ]+)', function(req, res) {
    proxy(req, res, req.params.path);
});

function downloadFile(sourcePath, savePath) {
    const file      = fs.createWriteStream(savePath);
    const isHTTP    = sourcePath.includes('http://');
    const client    = isHTTP ? http : https;
    const promise = new Promise((resolve, reject) => {
        client.get(sourcePath, function(response) {
            response.pipe(file);
            file.on('finish', () => {
                resolve();
            });
        });
    });
    return promise;
}

router.listen(3000, async() => {});
