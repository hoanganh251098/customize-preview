# README #

### What is this repository for? ###

Lấy lyrics trên Genius cho function text shape của sản phẩm customize

### Installation ###

This project requires NW.js to run.

Download NW.js: https://nwjs.io/downloads/

Copy all contents in `app/` to NW.js folder

Install the dependencies and devDependencies and start the server.

```sh
npm install
node index.js
```

Run the NW.js executable

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

[node.js]: <http://nodejs.org>